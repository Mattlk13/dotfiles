# Dotfiles
Dotfiles for my nix/unix-computers

## Screenshots
![Screenshot1](https://raw.githubusercontent.com/renegadevi/dotfiles/master/Screenshot_01.png)
![Screenshot2](https://raw.githubusercontent.com/renegadevi/dotfiles/master/Screenshot_02.png)

## Screenshot details

### Software
- OSX (10.11.5)
- iTerm2 (3.0 +borderless-padding)
- VIM (7.4 +python3)
- Tmux (2.3)
- Zsh (5.2)

#### Zsh plugins
- Oh-My-Zsh (https://github.com/robbyrussell/oh-my-zsh)
- zsh-autosuggestions (https://github.com/zsh-users/zsh-autosuggestions)
- zsh-syntax-highlighting (https://github.com/zsh-users/zsh-syntax-highlighting)
- zsh-notify (https://github.com/marzocchi/zsh-notify)
- enhancd (https://github.com/b4b4r07/enhancd)

### Terminal

- Font: 15pt Menlo Regular
- Background: #2A2D35
- Text color: #FFFFFF

#### ANSI Colors

| Color   | Normal  | Bright  |
|---------|---------|---------|
| Black   | #000000 | #707070 |
| Red     | #d16f56 | #e6aaa1 |
| Green   | #9fbe25 | #d6f065 |
| Yellow  | #d9bd2d | #ffe87b |
| Blue    | #86bfd7 | #acdff5 |
| Magenta | #d2b2dd | #fcc2fe |
| Cyan    | #3196a5 | #8ce3df |
| White   | #bebebe | #ffffff |

#### Extra
- Transparency: 5%
- Blur: 60%

### Vim

- Colorscheme: One (background=dark)
- Plugin manager: Pathogen.vim
- Plugins:
    - NERDTree
    - Python-syntax
    - Syntastic
    - Tagbar
    - Taghighlight
    - vim-airline
    - vim-airline-themes
    - vim-better-whitespace
    - vim-markdown
    - vim-nerdtree-tabs
- Syntax extras:
    - python.vim
